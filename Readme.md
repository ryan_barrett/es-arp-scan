Have docker and docker-compose installed.


### Arp-Scan Command

Set the <interface> and <IPrange>


```
# arp-scan --interface=<interface> <IPrange> > /dev/tcp/localhost/5049
```

Something like:
```
sudo arp-scan --interface=wlp3s0 10.10.10.0/24 > /dev/tcp/localhost/5049
```

or schedule with cron

```
9 *     * * *   root    /bin/bash -c "arp-scan --interface=enp0s25 10.10.10.0/24 > /dev/tcp/localhost/5049"
```



### Logstash Config

```
input {
 tcp {
    port => 5049   
    tags => ["arp"]
  }
}
filter {
  if "arp" in [tags] {
    grok {
       match => [ "message", '%{IP:ip_address}%{SPACE}%{MAC:mac_address}%{SPACE}%{GREEDYDATA:manufacturer}' ]
    }
    if "_grokparsefailure" in [tags]{drop {}}
  }
}
output {
  stdout{ codec => rubydebug }
 if "arp" in [tags] {
      elasticsearch {
        hosts => ["elasticsearch:9200"]
        index => "network-%{+YYYY.MM.dd}"
      }
  } else {
    elasticsearch {
        hosts => ["elasticsearch:9200"]
        index => "logstash-%{+YYYY.MM.dd}"
       }
  }
}
```

